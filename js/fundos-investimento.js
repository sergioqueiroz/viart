function menu(){
    $("#icon-menu").click(function(){
        if($(".sidebar").hasClass("full")){
            hideMenu()
        }
        else{
            showMenu();
        }       
    })
    $(".menu-item").click(function(){    
       if(!$(".sidebar").hasClass("full")){
            showMenu();
        }      
    })
    $(".has-sub-menu").click(function(){
        if($(this).hasClass("active")){
            $(this).removeClass("active");
            $(this).find(".icon-sub-menu i").removeClass("fa-chevron-up");
            $(this).find(".icon-sub-menu i").addClass("fa-chevron-down");
        }
        else{
            $(this).addClass("active");
            $(this).find(".icon-sub-menu i").removeClass("fa-chevron-down");
            $(this).find(".icon-sub-menu i").addClass("fa-chevron-up");
        } 
    })
}
function collapse(){
    $(".btn-collapse").click(function(){
        if($(this).parent().hasClass("expand")){
            $(this).parent().removeClass("expand");
            $(this).find("i.fas").removeClass("fa-chevron-up");
            $(this).find("i.fas").addClass("fa-chevron-down");
        }
        else{
            $(this).parent().addClass("expand");
            $(this).find("i.fas").removeClass("fa-chevron-down");
            $(this).find("i.fas").addClass("fa-chevron-up");
        } 
    })
}
function showMenu(){
    $(".show-text").show();
    $(".sidebar").addClass("full");
    $(".main").removeClass("full");
    $(".has-sub-menu").find(".icon-sub-menu").show();
}
function hideMenu(){
    $(".show-text").hide();
    $(".sidebar").removeClass("full");
    $(".main").addClass("full");
    $(".has-sub-menu").removeClass("active");
    $(".has-sub-menu").find(".icon-sub-menu").hide();
    $(".has-sub-menu").find(".icon-sub-menu i").removeClass("fa-chevron-up");
    $(".has-sub-menu").find(".icon-sub-menu i").addClass("fa-chevron-down");
}
/*CARROSEL*/

var carrosselBanner = (function () {
    var carrosselBanner = {};
    var Matriz = [];
    var itensListaCarrossel = [];
    var indiceCarrossel = 1;


    carrosselBanner.Iniciar = function () {
        var itens = $("#carrossel").children();

        if (itens.length > 1) {
            MontarArray(itens);
        }
    };

    function MontarArray(item) {
        item.map(function (index) {
            $(".bullet-carrossel").append('<li class="item-bullet"><a data-indice="' + index + '"><div class="circle-bullet"></div><a></li>');
        });
        itensListaCarrossel = $("#carrossel .item-carrossel").toArray();
        itensBulletCarrossel = $(".bullet-carrossel li.item-bullet").toArray();
        $(itensListaCarrossel[0]).addClass("active");
        $(itensBulletCarrossel[0]).addClass("active");
        carrosselLoop();

        //Click Bullets
        $(".bullet-carrossel .item-bullet a").click(function () {
            var indice = $(this).attr("data-indice");
            $("#carrossel .item-carrossel").removeClass("active");
            $(".bullet-carrossel .item-bullet").removeClass("active");
            $(itensListaCarrossel[indice]).addClass("active");
            $(itensBulletCarrossel[indice]).addClass("active");
            indiceCarrossel = indice;
            //carrosselLoop("",indice);
        })
    };
    function carrosselLoop() {
        setTimeout(function () {
            if (itensListaCarrossel.length > 1) {
                $("#carrossel .item-carrossel").removeClass("active");
                $(".bullet-carrossel .item-bullet").removeClass("active");
                $(itensListaCarrossel[indiceCarrossel]).addClass("active");
                $(itensBulletCarrossel[indiceCarrossel]).addClass("active");
                if (indiceCarrossel == (itensListaCarrossel.length - 1)) {
                    indiceCarrossel = 0;
                }
                else {
                    indiceCarrossel++;
                }
                carrosselLoop();
            }
        }, 10000)


    }

    return carrosselBanner;
}());

function marqueeInfinito(){
    $("#painel").append($(".painel").clone());
}


var selecionarTudo = (function () {
    var selecionarTudo = {};
    selecionarTudo.Iniciar = function () {

    
    };
    return selecionarTudo;
}());


$(document).ready(function(){
    menu();
    collapse();
    carrosselBanner.Iniciar();
    marqueeInfinito();
    selecionarTudo.Iniciar();
});